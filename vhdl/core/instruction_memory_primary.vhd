----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:03:05 05/12/2017 
-- Design Name: 
-- Module Name:    instruction_memory_primary - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.ISA.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instruction_memory_primary is
    Port ( addr : in  STD_LOGIC_VECTOR (6 downto 0);
           iout : out  STD_LOGIC_VECTOR (15 downto 0));
end instruction_memory_primary;

architecture Behavioral of instruction_memory_primary is
    constant T_RD : time := 2.5 ns;
    type mem_t is array (0 to 127) of std_logic_vector(15 downto 0);
    -- ##TAG_START_INST_MEM##
    signal mem : mem_t := (
        00 => inst_lwl(R_T2, R_H0),
        01 => inst_ldi(R_T12, 65),
        02 => inst_ldi(R_T13, 130),
        03 => inst_sub(R_T2, R_H0, R_H1),
        04 => inst_sw(R_T2, R_T12),
        05 => inst_add(R_T2, R_T12, R_H1),
        06 => inst_add(R_T5, R_T12, R_H1),
        07 => inst_sw(R_H0, R_T5),
        08 => inst_add(R_T0, R_H1, R_H1),
        09 => inst_lw(R_T3, R_T0),
        10 => inst_bne(R_T3, R_H0, 2),
        11 => inst_jmp(29),
        12 => inst_sub(R_T5, R_T0, R_H1),
        13 => inst_lw(R_T3, R_T5),
        14 => inst_lw(R_T4, R_T1),
        15 => inst_bne(R_T3, R_T4, 6),
        16 => inst_add(R_T1, R_T1, R_H1),
        17 => inst_add(R_T5, R_T12, R_T0),
        18 => inst_sw(R_T1, R_T5),
        19 => inst_add(R_T0, R_T0, R_H1),
        20 => inst_jmp(9),
        21 => inst_blt(R_T1, R_H1, 4),
        22 => inst_add(R_T5, R_T12, R_T1),
        23 => inst_lw(R_T1, R_T5),
        24 => inst_jmp(9),
        25 => inst_add(R_T5, R_T12, R_T0),
        26 => inst_sw(R_H0, R_T5),
        27 => inst_add(R_T0, R_T0, R_H1),
        28 => inst_jmp(9),
        29 => inst_swu(R_H0, R_T13),
        30 => inst_nop,
        31 => inst_nop,
        32 => inst_nop,
        33 => inst_nop,
        34 => inst_nop,
        35 => inst_nop,
        36 => inst_nop,
        37 => inst_nop,
        38 => inst_add(R_T7, R_H0, R_H0),
        39 => inst_rio(R_T8),
        40 => inst_bne(R_T8, R_H0, 2),
        41 => inst_jmp(74),
        42 => inst_lw(R_T9, R_T7),
        43 => inst_bne(R_T8, R_T9, 2),
        44 => inst_jmp(58),
        45 => inst_add(R_T5, R_T12, R_T7),
        46 => inst_lw(R_T10, R_T5),
        47 => inst_sub(R_T2, R_T7, R_T10),
        48 => inst_blt(R_T10, R_H0, 7),
        49 => inst_add(R_T7, R_T10, R_H0),
        50 => inst_bne(R_T2, R_H0, 4),
        51 => inst_sub(R_T2, R_T2, R_H1),
        52 => inst_rio(R_T8),
        53 => inst_jmp(50),
        54 => inst_jmp(40),
        55 => inst_add(R_T7, R_H0, R_H0),
        56 => inst_rio(R_T8),
        57 => inst_jmp(40),
        58 => inst_add(R_T5, R_T7, R_H1),
        59 => inst_lw(R_T9, R_T5),
        60 => inst_beq(R_T9, R_H0, 4),
        61 => inst_add(R_T7, R_T7, R_H1),
        62 => inst_rio(R_T8),
        63 => inst_jmp(40),
        64 => inst_lwl(R_T2, R_T13),
        65 => inst_add(R_T2, R_T2, R_H1),
        66 => inst_swu(R_T2, R_T13),
        67 => inst_add(R_T5, R_T12, R_T7),
        68 => inst_lw(R_T2, R_T5),
        69 => inst_blt(R_H0, R_T2, 3),
        70 => inst_add(R_T7, R_H0, R_H0),
        71 => inst_jmp(40),
        72 => inst_add(R_T7, R_T2, R_H0),
        73 => inst_jmp(40),
        74 => inst_nop,
        75 => inst_jmp(74),
        others => x"0000"
    );
	-- ##TAG_END_INST_MEM##

begin

    iout <= mem(to_integer(unsigned(addr))) after T_RD;

end Behavioral;

