----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:23:42 04/12/2017 
-- Design Name: 
-- Module Name:    pipeline_if_id - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_if_id is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           en : in  STD_LOGIC;
           zero : in  STD_LOGIC;
           next_inst : in  STD_LOGIC_VECTOR (15 downto 0);
           next_pc : in  STD_LOGIC_VECTOR (6 downto 0);
           curr_inst : out  STD_LOGIC_VECTOR (15 downto 0);
           curr_pc : out  STD_LOGIC_VECTOR (6 downto 0));
end pipeline_if_id;

architecture Behavioral of pipeline_if_id is
    -- 23 bits: inst(16b) + pc(7b)
    signal mem : std_logic_vector(22 downto 0);
begin

    curr_inst <= mem(22 downto 7);
    curr_pc <= mem(6 downto 0);

    process(clk, rst)
    begin
        if rst = '1' then
            mem <= (others => '0');
        elsif rising_edge(clk) then
            if en = '1' then
                if zero = '1' then
                    mem <= (others => '0');
                else
                    mem <= next_inst
                         & next_pc;
                end if;
            end if;
        end if;
    end process;

end Behavioral;

