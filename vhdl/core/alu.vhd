----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:06:34 04/12/2017 
-- Design Name: 
-- Module Name:    alu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu is
    Port ( arg_a : in  STD_LOGIC_VECTOR (7 downto 0);
           arg_b : in  STD_LOGIC_VECTOR (7 downto 0);
           sub : in  STD_LOGIC;
           result : out  STD_LOGIC_VECTOR (7 downto 0);
           zero : out  STD_LOGIC;
           msb : out  STD_LOGIC);
end alu;

architecture Behavioral of alu is
    constant T_RES : time := 1ns;
    signal ua, ub, ur : unsigned(7 downto 0);
    signal res : std_logic_vector(7 downto 0);
begin

    ua <= unsigned(arg_a);
    ub <= unsigned(arg_b);
    ur <= ua - ub when sub = '1' else ua + ub;
    res <= std_logic_vector(ur) after T_RES;
    result <= res;
    msb <= res(7);
    zero <= '1' when res = x"00" else '0';

end Behavioral;

