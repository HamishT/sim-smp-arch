----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:07:41 05/09/2017 
-- Design Name: 
-- Module Name:    data_cache - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_cache is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           proc_rden : in  STD_LOGIC;
           proc_wren : in  STD_LOGIC;
           proc_stall : out  STD_LOGIC;
           proc_addr : in  STD_LOGIC_VECTOR (7 downto 0);
           proc_dout : out  STD_LOGIC_VECTOR (7 downto 0);
           bus_req : out  STD_LOGIC;
           bus_grant : in  STD_LOGIC;
           bus_din : in  STD_LOGIC_VECTOR (7 downto 0));
end data_cache;

architecture Behavioral of data_cache is
    
    constant T_CHECK : time := 1.8 ns; -- valid bit check
    constant T_READ : time := 1.8 ns; -- cache line load
    constant T_GATE : time := 0.2 ns; -- 4 fan-in gate
    constant T_MUX : time := 0.2 ns;
    
    type mem_t is array (0 to 127) of std_logic_vector(7 downto 0);
    signal mem_data : mem_t := (others => (others => '0'));
    signal mem_valid : std_logic_vector(0 to 127) := (others => '0');
    signal is_updating : std_logic := '0'; -- essentially a 2-state machine
    
    signal miss, bypass, bus_req_sig : std_logic := '0';

    signal curr_index : integer := 0;
    signal curr_line : std_logic_vector(7 downto 0);
    signal curr_valid : std_logic;
    
begin

    curr_index <= to_integer(unsigned(proc_addr(6 downto 0)));
    curr_valid <= mem_valid(curr_index) after T_CHECK;
    curr_line <= mem_data(curr_index) after T_READ;

    ---- cache logic equations
    ---- b (bypass), m (miss), c (bus req), s (stall), u(updating)
    ---- w (wren), r (rden), a (addr), v (valid), g (grant)
    -- b = w + ra7
    -- m = r~v~a7
    -- c = b + u
    -- s = ~g(b + u) + m
    -- u = (u~g + ~um) on posedge
    --
    ---- Timing assumes a "4 fan-in gate" can be any 4-variable function,
    ---- i.e. like a real-life 4-LUT.
                
    bypass <= proc_wren or (proc_rden and proc_addr(7))
              after T_GATE;
    
    miss <= proc_rden and not proc_addr(7) and not curr_valid
            after T_GATE;
            
    -- this is still a 4-input function and bypass already has a delay
    -- so no extra delay is applied here
    bus_req_sig <= bypass or is_updating;
    bus_req <= bus_req_sig;
               
    proc_stall <= miss or (not bus_grant and (bypass or is_updating))
                  after T_GATE;
    
    proc_dout <= bus_din when bus_req_sig = '1' else curr_line
                 after T_MUX;

    process (clk, rst)
    begin
        if rst = '1' then
            is_updating <= '0';
            mem_valid <= (others => '0');
            mem_data <= (others => (others => '0'));
        elsif rising_edge(clk) then
            if is_updating = '0' and miss = '1' then
                is_updating <= '1';
            elsif is_updating = '1' and bus_grant = '1' then
                is_updating <= '0';
                mem_data(curr_index) <= bus_din;
                mem_valid(curr_index) <= '1';
            end if;
        end if;
    end process;

end Behavioral;

