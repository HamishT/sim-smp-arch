----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:01:54 04/12/2017 
-- Design Name: 
-- Module Name:    control_unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library WORK;
use WORK.ISA.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control_unit is
    Port ( op : in  STD_LOGIC_VECTOR (3 downto 0);
           jump : out  STD_LOGIC;
           io_rden : out  STD_LOGIC;
           reg_wren : out  STD_LOGIC;
           mem_rden : out  STD_LOGIC;
           mem_wren : out  STD_LOGIC;
           bus_lock : out  STD_LOGIC;
           bus_unlock : out  STD_LOGIC;
           mem_to_reg : out  STD_LOGIC;
           sub : out  STD_LOGIC;
           use_imm : out  STD_LOGIC;
           beq : out  STD_LOGIC;
           bne : out  STD_LOGIC;
           blt : out  STD_LOGIC);
end control_unit;

architecture Behavioral of control_unit is
    
    constant T_CTRL : time := 1.5ns; -- specified in project spec

begin

    io_rden <= '1' when op = OP_RIO
                   else '0' after T_CTRL;
    reg_wren <= '1' when op = OP_ADD or
                         op = OP_SUB or
                         op = OP_LW or
                         op = OP_LWL or
                         op = OP_RIO or
                         op = OP_LDI
                    else '0' after T_CTRL;
    mem_rden <= '1' when op = OP_LW or
                         op = OP_LWL
                    else '0' after T_CTRL;
    mem_wren <= '1' when op = OP_SW or
                         op = OP_SWU
                    else '0' after T_CTRL;
    bus_lock <= '1' when op = OP_LWL
                    else '0' after T_CTRL;
    bus_unlock <= '1' when op = OP_SWU
                      else '0' after T_CTRL;
    mem_to_reg <= '1' when op = OP_LW or
                           op = OP_LWL
                   else '0' after T_CTRL;
    sub <= '1' when op = OP_SUB or
                    op = OP_BEQ or
                    op = OP_BNE or
                    op = OP_BLT
               else '0' after T_CTRL;
    use_imm <= '1' when op = OP_LDI
                   else '0' after T_CTRL;
    beq <= '1' when op = OP_BEQ
               else '0' after T_CTRL;
    bne <= '1' when op = OP_BNE
               else '0' after T_CTRL;
    blt <= '1' when op = OP_BLT
               else '0' after T_CTRL;
    jump <= '1' when op = OP_JMP
                else '0' after T_CTRL;

end Behavioral;

