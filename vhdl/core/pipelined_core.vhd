----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:55:13 04/12/2017 
-- Design Name: 
-- Module Name:    pipelined_core - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipelined_core is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           inst_addr_out : out  STD_LOGIC_VECTOR (6 downto 0);
           inst_data_in : in  STD_LOGIC_VECTOR (15 downto 0);
           io_rden : out  STD_LOGIC;
           io_data : in  STD_LOGIC_VECTOR (7 downto 0);
           mem_addr_out : out  STD_LOGIC_VECTOR (7 downto 0);
           mem_data_in :  in  STD_LOGIC_VECTOR (7 downto 0);
           mem_data_out : out  STD_LOGIC_VECTOR (7 downto 0);
           mem_wren : out  STD_LOGIC;
           bus_req : out  STD_LOGIC;
           bus_req_lock : out  STD_LOGIC;
           bus_req_unlock : out  STD_LOGIC;
           bus_grant : in  STD_LOGIC);
end pipelined_core;

architecture Behavioral of pipelined_core is
    component mux8x2
        Port ( a : in  STD_LOGIC_VECTOR (7 downto 0);
               b : in  STD_LOGIC_VECTOR (7 downto 0);
               s : in  STD_LOGIC;
               o : out  STD_LOGIC_VECTOR (7 downto 0));
    end component;
    component mux7x2
        Port ( a : in  STD_LOGIC_VECTOR (6 downto 0);
               b : in  STD_LOGIC_VECTOR (6 downto 0);
               s : in  STD_LOGIC;
               o : out  STD_LOGIC_VECTOR (6 downto 0));
    end component;
    component alu
        Port ( arg_a : in  STD_LOGIC_VECTOR (7 downto 0);
               arg_b : in  STD_LOGIC_VECTOR (7 downto 0);
               sub : in  STD_LOGIC;
               result : out  STD_LOGIC_VECTOR (7 downto 0);
               zero : out  STD_LOGIC;
               msb : out  STD_LOGIC);
    end component;
    component program_counter
        generic ( WIDTH : natural := 7;
                  BRANCH_WIDTH : natural := 4);
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               en : in  STD_LOGIC;
               pcout : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
               branch : in  STD_LOGIC;
               target : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
    end component;
    component control_unit
        Port ( op : in  STD_LOGIC_VECTOR (3 downto 0);
               jump : out  STD_LOGIC;
               io_rden : out  STD_LOGIC;
               reg_wren : out  STD_LOGIC;
               mem_rden : out  STD_LOGIC;
               mem_wren : out  STD_LOGIC;
               bus_lock : out  STD_LOGIC;
               bus_unlock : out  STD_LOGIC;
               mem_to_reg : out  STD_LOGIC;
               sub : out  STD_LOGIC;
               use_imm : out  STD_LOGIC;
               beq : out  STD_LOGIC;
               bne : out  STD_LOGIC;
               blt : out  STD_LOGIC);
    end component;
    component hazard_controller
        Port ( enable : in  STD_LOGIC;
               id_a_addr : in  STD_LOGIC_VECTOR (3 downto 0);
               id_b_addr : in  STD_LOGIC_VECTOR (3 downto 0);
               ex_wr_addr : in  STD_LOGIC_VECTOR (3 downto 0);
               fwd_sel_a : out  STD_LOGIC;
               fwd_sel_b : out  STD_LOGIC);
    end component;
    component register_file
        generic ( SIZEB : natural := 4; -- must = log2(sizeT)
                  SIZET : natural := 16; -- must = 2^sizeB
                  WIDTH : natural := 8);
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               addr_rd_a : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
               addr_rd_b : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
               addr_wr : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
               wren : in  STD_LOGIC;
               din : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
               dout_a : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
               dout_b : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
    end component;
    component data_cache
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               proc_rden : in  STD_LOGIC;
               proc_wren : in  STD_LOGIC;
               proc_stall : out  STD_LOGIC;
               proc_addr : in  STD_LOGIC_VECTOR (7 downto 0);
               proc_dout : out  STD_LOGIC_VECTOR (7 downto 0);
               bus_req : out  STD_LOGIC;
               bus_grant : in  STD_LOGIC;
               bus_din : in  STD_LOGIC_VECTOR (7 downto 0));
    end component;
    component pipeline_if_id
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               en : in  STD_LOGIC;
               zero : in  STD_LOGIC;
               next_inst : in  STD_LOGIC_VECTOR (15 downto 0);
               next_pc : in  STD_LOGIC_VECTOR (6 downto 0);
               curr_inst : out  STD_LOGIC_VECTOR (15 downto 0);
               curr_pc : out  STD_LOGIC_VECTOR (6 downto 0));
    end component;
    component pipeline_id_ex
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               en : in  STD_LOGIC;
               zero : in  STD_LOGIC;
               
               next_reg_a_data : in  STD_LOGIC_VECTOR (7 downto 0);
               next_reg_b_data : in  STD_LOGIC_VECTOR (7 downto 0);
               next_imm_val : in  STD_LOGIC_VECTOR (7 downto 0);
               next_reg_wr_addr : in  STD_LOGIC_VECTOR (3 downto 0);
               next_sub : in  STD_LOGIC;
               next_use_imm : in  STD_LOGIC;
               next_mem_rden : in  STD_LOGIC;
               next_mem_wren :  in  STD_LOGIC;
               next_lock : in  STD_LOGIC;
               next_unlock : in  STD_LOGIC;
               next_mem_to_reg :  in  STD_LOGIC;
               next_reg_wren : in  STD_LOGIC;
               next_pc_rel : in  STD_LOGIC_VECTOR (6 downto 0);
               next_beq : in  STD_LOGIC;
               next_bne : in  STD_LOGIC;
               next_blt : in  STD_LOGIC;
               
               curr_reg_a_data : out  STD_LOGIC_VECTOR (7 downto 0);
               curr_reg_b_data : out  STD_LOGIC_VECTOR (7 downto 0);
               curr_imm_val : out  STD_LOGIC_VECTOR (7 downto 0);
               curr_reg_wr_addr : out  STD_LOGIC_VECTOR (3 downto 0);
               curr_sub : out  STD_LOGIC;
               curr_use_imm : out  STD_LOGIC;
               curr_mem_rden : out  STD_LOGIC;
               curr_mem_wren : out  STD_LOGIC;
               curr_lock : out  STD_LOGIC;
               curr_unlock : out  STD_LOGIC;
               curr_mem_to_reg : out  STD_LOGIC;
               curr_reg_wren : out  STD_LOGIC;
               curr_pc_rel : out  STD_LOGIC_VECTOR (6 downto 0);
               curr_beq : out  STD_LOGIC;
               curr_bne : out  STD_LOGIC;
               curr_blt : out  STD_LOGIC);
    end component;
    component pipeline_ex_wb
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               en : in  STD_LOGIC;
               zero : in  STD_LOGIC;
               
               next_reg_data : in std_logic_vector(7 downto 0);
               next_reg_addr : in std_logic_vector(3 downto 0);
               next_reg_wren : in std_logic;
               
               curr_reg_data : out std_logic_vector(7 downto 0);
               curr_reg_addr : out std_logic_vector(3 downto 0);
               curr_reg_wren : out std_logic);
    end component;
    
    signal ctrl_jump, ctrl_rio, ctrl_sub, ctrl_mem_rden, ctrl_mem_wren : std_logic;
    signal ctrl_reg_wren, ctrl_lock, ctrl_unlock, ctrl_mem_to_reg, ctrl_use_imm : std_logic;
    signal ctrl_beq, ctrl_bne, ctrl_blt : std_logic;
    
    signal cache_stall : std_logic;
    
    signal ifid_zero, ifid_en, idex_zero, idex_en, exwb_zero, exwb_en : std_logic;
    
    signal if_pc_en : std_logic;
    signal if_pc : std_logic_vector(6 downto 0);
    signal if_inst : std_logic_vector(15 downto 0);
    signal if_branch_do : std_logic;
    signal if_branch_target : std_logic_vector(6 downto 0);
    
    signal id_inst : std_logic_vector(15 downto 0);
    signal id_opcode, id_wr_addr, id_a_addr, id_b_addr, id_branch_offs : std_logic_vector (3 downto 0);
    signal id_a_reg_val, id_b_reg_val, id_a_data, id_b_data, id_a_iomux, id_imm_val : std_logic_vector(7 downto 0);
    signal id_pc, id_branch_pc_abs, id_branch_pc_rel : std_logic_vector(6 downto 0);
    signal id_alu_a, id_alu_b, id_alu_o : std_logic_vector(7 downto 0);
    signal id_fwd_a, id_fwd_b : std_logic;
    
    signal ex_a_data, ex_b_data, ex_alu_res, ex_mem_data, ex_imm_val, ex_res_val, ex_wb_data : std_logic_vector(7 downto 0);
    signal ex_sub, ex_mem_rden, ex_mem_wren, ex_lock, ex_unlock : std_logic;
    signal ex_alu_msb, ex_alu_zero : std_logic;
    signal ex_beq, ex_bne, ex_blt, ex_branch_do : std_logic;
    signal ex_reg_wr_addr : std_logic_vector(3 downto 0);
    signal ex_branch_pc_rel : std_logic_vector(6 downto 0);
    signal ex_reg_wren, ex_mem_to_reg, ex_use_imm : std_logic;
    
    signal wb_data : std_logic_vector(7 downto 0);
    signal wb_addr : std_logic_vector(3 downto 0);
    signal wb_wren : std_logic;
    
    constant T_EX_BRANCH_EVAL : time := 0.4 ns; -- 2 4fin logic levels

begin

    -- EXTERNAL IO
    
    io_rden <= ctrl_rio and (cache_stall nor if_branch_do); -- prevents accidental character skips
    
    mem_addr_out <= ex_a_data;
    mem_data_out <= ex_b_data;
    mem_wren <= ex_mem_wren;
    
    bus_req_lock <= ex_lock;
    bus_req_unlock <= ex_unlock;
    
    inst_addr_out <= if_pc;
    if_inst <= inst_data_in;

    -- PIPELINE CONTROL SIGNALS
    
    if_pc_en <= not cache_stall;
    
    ifid_zero <= if_branch_do; -- flush mispredicted instruction on jump
    ifid_en  <= not cache_stall;
    
    idex_zero <= if_branch_do; -- flush jump instruction itself
    idex_en  <= not cache_stall;
    
    exwb_zero <= '0';
    exwb_en  <= not cache_stall;

    -- CONTROLS
    
    ctrl : control_unit port map (
        op          => id_opcode,
        jump        => ctrl_jump,
        io_rden     => ctrl_rio,
        reg_wren    => ctrl_reg_wren,
        mem_rden    => ctrl_mem_rden,
        mem_wren    => ctrl_mem_wren,
        bus_lock    => ctrl_lock,
        bus_unlock  => ctrl_unlock,
        mem_to_reg  => ctrl_mem_to_reg,
        sub         => ctrl_sub,
        use_imm     => ctrl_use_imm,
        beq         => ctrl_beq,
        bne         => ctrl_bne,
        blt         => ctrl_blt);
        
    hazard : hazard_controller port map (
        enable     => ex_reg_wren,
        id_a_addr  => id_a_addr,
        id_b_addr  => id_b_addr,
        ex_wr_addr => ex_reg_wr_addr,
        fwd_sel_a  => id_fwd_a,
        fwd_sel_b  => id_fwd_b);

    -- IF STAGE
    
    if_branch_do <= ctrl_jump or ex_branch_do;
    
    if_target_mux : mux7x2 port map (
        a => id_branch_pc_abs,
        b => ex_branch_pc_rel,
        s => ex_branch_do,
        o => if_branch_target);
    
    if_counter : program_counter port map (
        clk      => clk,
        rst      => rst,
        en       => if_pc_en,
        pcout    => if_pc,
        branch   => if_branch_do,
        target   => if_branch_target);
        
    -- IF/ID
    
    pipe_ifid : pipeline_if_id port map (
        clk       => clk,
        rst       => rst,
        en        => ifid_en,
        zero      => ifid_zero,
        next_inst => if_inst,
        next_pc   => if_pc,
        curr_inst => id_inst,
        curr_pc   => id_pc);
        
    -- ID STAGE
    
    id_opcode <= id_inst(15 downto 12);
    id_branch_pc_abs <= id_inst(6 downto 0);
    id_branch_offs <= id_inst(11 downto 8);
    id_wr_addr <= id_inst(11 downto 8);
    id_a_addr <= id_inst(7 downto 4);
    id_b_addr <= id_inst(3 downto 0);
    id_imm_val <= id_inst(7 downto 0);
    
    id_regfile : register_file port map (
        clk       => clk,
        rst       => rst,
        addr_rd_a => id_a_addr,
        addr_rd_b => id_b_addr,
        addr_wr   => wb_addr,
        wren      => wb_wren,
        din       => wb_data,
        dout_a    => id_a_reg_val,
        dout_b    => id_b_reg_val);
        
    id_alu_a <= "0" & id_pc;
    id_alu_b <= "0000" & id_branch_offs;
    id_branch_pc_rel <= id_alu_o(6 downto 0);
        
    id_branch_alu : alu port map (
        arg_a              => id_alu_a,
        arg_b              => id_alu_b,
        result             => id_alu_o,
        sub                => '0',
        zero               => open,
        msb                => open);
        
    id_a_mux : mux8x2 port map (
        a => id_a_reg_val,
        b => ex_wb_data,
        s => id_fwd_a,
        o => id_a_data);
        
    id_b_mux : mux8x2 port map (
        a => id_b_reg_val,
        b => ex_wb_data,
        s => id_fwd_b,
        o => id_b_data);
        
    id_io_mux : mux8x2 port map (
        a => id_a_data,
        b => io_data,
        s => ctrl_rio,
        o => id_a_iomux);
        
    -- ID/EX
    
    pipe_idex : pipeline_id_ex port map (
        clk              => clk,
        rst              => rst,
        en               => idex_en,
        zero             => idex_zero,
        
        next_reg_a_data  => id_a_iomux,
        next_reg_b_data  => id_b_data,
        next_imm_val     => id_imm_val,
        next_reg_wr_addr => id_wr_addr,
        next_reg_wren    => ctrl_reg_wren,
        next_sub         => ctrl_sub,
        next_use_imm     => ctrl_use_imm,
        next_mem_rden    => ctrl_mem_rden,
        next_mem_wren    => ctrl_mem_wren,
        next_lock        => ctrl_lock,
        next_unlock      => ctrl_unlock,
        next_mem_to_reg  => ctrl_mem_to_reg,
        next_pc_rel      => id_branch_pc_rel,
        next_beq         => ctrl_beq,
        next_bne         => ctrl_bne,
        next_blt         => ctrl_blt,
        
        curr_reg_a_data  => ex_a_data,
        curr_reg_b_data  => ex_b_data,
        curr_imm_val     => ex_imm_val,
        curr_reg_wr_addr => ex_reg_wr_addr,
        curr_reg_wren    => ex_reg_wren,
        curr_sub         => ex_sub,
        curr_use_imm     => ex_use_imm,
        curr_mem_rden    => ex_mem_rden,
        curr_mem_wren    => ex_mem_wren,
        curr_lock        => ex_lock,
        curr_unlock      => ex_unlock,
        curr_mem_to_reg  => ex_mem_to_reg,
        curr_pc_rel      => ex_branch_pc_rel,
        curr_beq         => ex_beq,
        curr_bne         => ex_bne,
        curr_blt         => ex_blt);
         
    -- EX STAGE
    
    ex_branch_do <= (ex_beq and ex_alu_zero) or
                    (ex_bne and not ex_alu_zero) or
                    (ex_blt and ex_alu_msb)
                    after T_EX_BRANCH_EVAL;
    
    ex_alu : alu port map (
        arg_a  => ex_a_data,
        arg_b  => ex_b_data,
        sub    => ex_sub,
        result => ex_alu_res,
        zero   => ex_alu_zero,
        msb    => ex_alu_msb);
        
    ex_dcache : data_cache port map (
        clk        => clk,
        rst        => rst,
        proc_rden  => ex_mem_rden,
        proc_wren  => ex_mem_wren,
        proc_stall => cache_stall,
        proc_addr  => ex_a_data,
        proc_dout  => ex_mem_data,
        bus_req    => bus_req,
        bus_grant  => bus_grant,
        bus_din    => mem_data_in
    );
        
    ex_res_mux : mux8x2 port map (
        a => ex_alu_res,
        b => ex_mem_data,
        s => ex_mem_to_reg,
        o => ex_res_val);
        
    ex_final_mux : mux8x2 port map (
        a => ex_res_val,
        b => ex_imm_val,
        s => ex_use_imm,
        o => ex_wb_data);
        
    -- EX/WB STAGE
    
    pipe_exwb: pipeline_ex_wb port map (
       clk           => clk,
       rst           => rst,
       en            => exwb_en,
       zero          => exwb_zero,
       
       next_reg_data => ex_wb_data,
       next_reg_addr => ex_reg_wr_addr,
       next_reg_wren => ex_reg_wren,
       
       curr_reg_data => wb_data,
       curr_reg_addr => wb_addr,
       curr_reg_wren => wb_wren);
    
    -- WB STAGE
    -- no actual components, but pipeline register write values are important

end Behavioral;

