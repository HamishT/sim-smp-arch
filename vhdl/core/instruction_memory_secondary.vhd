----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:05:40 05/12/2017 
-- Design Name: 
-- Module Name:    instruction_memory_secondary - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.ISA.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instruction_memory_secondary is
    Port ( addr : in  STD_LOGIC_VECTOR (6 downto 0);
           iout : out  STD_LOGIC_VECTOR (15 downto 0));
end instruction_memory_secondary;

architecture Behavioral of instruction_memory_secondary is
    constant T_RD : time := 2.5 ns;
    type mem_t is array (0 to 127) of std_logic_vector(15 downto 0);
    -- ##TAG_START_INST_MEM##
    signal mem : mem_t := (
        00 => inst_ldi(R_T5, 255),
        01 => inst_ldi(R_T12, 65),
        02 => inst_ldi(R_T13, 130),
        03 => inst_lwl(R_T2, R_H0),
        04 => inst_swu(R_H1, R_T5),
        05 => inst_add(R_T7, R_H0, R_H0),
        06 => inst_rio(R_T8),
        07 => inst_bne(R_T8, R_H0, 2),
        08 => inst_jmp(41),
        09 => inst_lw(R_T9, R_T7),
        10 => inst_bne(R_T8, R_T9, 2),
        11 => inst_jmp(25),
        12 => inst_add(R_T5, R_T12, R_T7),
        13 => inst_lw(R_T10, R_T5),
        14 => inst_sub(R_T2, R_T7, R_T10),
        15 => inst_blt(R_T10, R_H0, 7),
        16 => inst_add(R_T7, R_T10, R_H0),
        17 => inst_bne(R_T2, R_H0, 4),
        18 => inst_sub(R_T2, R_T2, R_H1),
        19 => inst_rio(R_T8),
        20 => inst_jmp(17),
        21 => inst_jmp(7),
        22 => inst_add(R_T7, R_H0, R_H0),
        23 => inst_rio(R_T8),
        24 => inst_jmp(7),
        25 => inst_add(R_T5, R_T7, R_H1),
        26 => inst_lw(R_T9, R_T5),
        27 => inst_beq(R_T9, R_H0, 4),
        28 => inst_add(R_T7, R_T7, R_H1),
        29 => inst_rio(R_T8),
        30 => inst_jmp(7),
        31 => inst_lwl(R_T2, R_T13),
        32 => inst_add(R_T2, R_T2, R_H1),
        33 => inst_swu(R_T2, R_T13),
        34 => inst_add(R_T5, R_T12, R_T7),
        35 => inst_lw(R_T2, R_T5),
        36 => inst_blt(R_H0, R_T2, 3),
        37 => inst_add(R_T7, R_H0, R_H0),
        38 => inst_jmp(7),
        39 => inst_add(R_T7, R_T2, R_H0),
        40 => inst_jmp(7),
        41 => inst_nop,
        42 => inst_jmp(41),
        others => x"0000"
    );
    -- ##TAG_END_INST_MEM##
    
begin

    iout <= mem(to_integer(unsigned(addr))) after T_RD;

end Behavioral;

