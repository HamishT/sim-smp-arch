----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:40:38 04/11/2017 
-- Design Name: 
-- Module Name:    data_memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.TEXTIO.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_memory is
    generic ( SIZEB : natural := 8; -- must be log2(sizeT)
              SIZET : natural := 256; -- must be 2^sizeB
              WIDTH : natural := 8);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           dout : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           addr : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
           wren : in  STD_LOGIC);
end data_memory;

architecture Behavioral of data_memory is

    type mem_t is array (0 to SIZET-1) of std_logic_vector(WIDTH-1 downto 0);

    impure function mem_init (constant pattern_file : in string) return mem_t is
        file infile : text is in pattern_file;
        variable inline : line;
        variable inchar : character;
        variable index : integer := 0;
        variable outmem : mem_t := (others => (others => '0'));
    begin
        readline(infile, inline);
        while inline'length /= 0 loop
            read(inline, inchar);
            outmem(index) := std_logic_vector(to_unsigned(character'pos(inchar), 8));
            index := index + 1;
        end loop;
        return outmem;
    end function;

    constant T_WR : time := 2.3ns; -- spec: T for 8N words = 0.3N + 0.4log2(N) [N = 32 for 256 words]
    constant T_RD : time := 2.3ns; -- note: ask tutor/lecturer about how times apply
    signal mem : mem_t := mem_init("..\..\text\input_pattern.txt");
    
begin
	
    dout <= mem(to_integer(unsigned(addr))) after T_RD;

    write_proc: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                -- do nothing: array was already 
            elsif wren = '1' then
                mem(to_integer(unsigned(addr))) <= din;
            end if;
        end if;
    end process;

end Behavioral;

