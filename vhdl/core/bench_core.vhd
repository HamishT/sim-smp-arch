--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:57:31 04/19/2017
-- Design Name:   
-- Module Name:   C:/Users/Hamish/Desktop/cs3211/cs3211-proj/vhdl/core/bench_core.vhd
-- Project Name:  PipelinedProc
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: pipelined_core
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bench_core IS
END bench_core;
 
ARCHITECTURE behavior OF bench_core IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT pipelined_core
        PORT ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               inst_addr_out : out  STD_LOGIC_VECTOR (6 downto 0);
               inst_data_in : in  STD_LOGIC_VECTOR (15 downto 0);
               io_rden : out  STD_LOGIC;
               io_data : in  STD_LOGIC_VECTOR (7 downto 0);
               mem_addr_out : out  STD_LOGIC_VECTOR (7 downto 0);
               mem_data_in :  in  STD_LOGIC_VECTOR (7 downto 0);
               mem_data_out : out  STD_LOGIC_VECTOR (7 downto 0);
               mem_wren : out  STD_LOGIC;
               bus_req : out  STD_LOGIC;
               bus_req_lock : out  STD_LOGIC;
               bus_req_unlock : out  STD_LOGIC;
               bus_grant : in  STD_LOGIC);
    END COMPONENT;
    
    component io_buffer
        Generic ( text_file : string := "" );
        Port ( clk      : in  STD_LOGIC;
               enable   : in  STD_LOGIC;
               data_out : out  STD_LOGIC_VECTOR(7 downto 0));
    end component;
    
    component instruction_memory_primary
        Port ( addr : in  STD_LOGIC_VECTOR (6 downto 0);
               iout : out  STD_LOGIC_VECTOR (15 downto 0));
    end component;
    
    component instruction_memory_secondary
        Port ( addr : in  STD_LOGIC_VECTOR (6 downto 0);
               iout : out  STD_LOGIC_VECTOR (15 downto 0));
    end component;
    
    component data_memory
        generic ( SIZEB : natural := 8; -- must be log2(sizeT)
                  SIZET : natural := 256; -- must be 2^sizeB
                  WIDTH : natural := 8);
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               din : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
               dout : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
               addr : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
               wren : in  STD_LOGIC);
    end component;
    
    component data_bus
        Port ( clk : in  STD_LOGIC;
               rst : in  STD_LOGIC;
               req : in  STD_LOGIC_VECTOR (1 downto 0);
               req_lock : in  STD_LOGIC_VECTOR (1 downto 0);
               req_unlock : in  STD_LOGIC_VECTOR (1 downto 0);
               wren : in  STD_LOGIC_VECTOR (1 downto 0);
               grant : out  STD_LOGIC_VECTOR (1 downto 0);
               addr_in0 : in  STD_LOGIC_VECTOR (7 downto 0);
               addr_in1 : in  STD_LOGIC_VECTOR (7 downto 0);
               data_in0 : in  STD_LOGIC_VECTOR (7 downto 0);
               data_in1 : in  STD_LOGIC_VECTOR (7 downto 0);
               wren_out : out  STD_LOGIC;
               addr_out : out  STD_LOGIC_VECTOR (7 downto 0);
               data_out : out  STD_LOGIC_VECTOR (7 downto 0));
    end component;

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '1';

    --IO buffer signals
   signal io_rden_p, io_rden_s : std_logic := '0';
   signal io_data_p, io_data_s : std_logic_vector(7 downto 0) := x"00";

   -- Clock period definitions
   constant clk_period : time := 5 ns;
   
   signal inst_addr_p, inst_addr_s : std_logic_vector(6 downto 0);
   signal inst_data_p, inst_data_s : std_logic_vector(15 downto 0);
   signal mem_addr_p, mem_dout_p, mem_addr_s, mem_dout_s, mem_data_read : std_logic_vector(7 downto 0);
   signal mem_wren_p, bus_req_p, bus_lock_p, bus_unlock_p, bus_grant_p : std_logic;
   signal mem_wren_s, bus_req_s, bus_lock_s, bus_unlock_s, bus_grant_s : std_logic;
   signal bus_mem_addr, bus_mem_data : std_logic_vector(7 downto 0);
   signal bus_mem_wren : std_logic;
 
BEGIN
 
    core_primary : pipelined_core PORT MAP (
        clk => clk,
        rst => rst,
        inst_addr_out => inst_addr_p,
        inst_data_in => inst_data_p,
        io_rden => io_rden_p,
        io_data => io_data_p,
        mem_addr_out => mem_addr_p,
        mem_data_in  => mem_data_read,
        mem_data_out => mem_dout_p,
        mem_wren => mem_wren_p,
        bus_req => bus_req_p,
        bus_req_lock => bus_lock_p,
        bus_req_unlock => bus_unlock_p,
        bus_grant => bus_grant_p
    );
    
    core_secondary : pipelined_core PORT MAP (
        clk => clk,
        rst => rst,
        inst_addr_out => inst_addr_s,
        inst_data_in => inst_data_s,
        io_rden => io_rden_s,
        io_data => io_data_s,
        mem_addr_out => mem_addr_s,
        mem_data_in  => mem_data_read,
        mem_data_out => mem_dout_s,
        mem_wren => mem_wren_s,
        bus_req => bus_req_s,
        bus_req_lock => bus_lock_s,
        bus_req_unlock => bus_unlock_s,
        bus_grant => bus_grant_s
    );
        
    iobuffer_primary : io_buffer
    generic map (
        text_file => "../../text/input_text_primary.txt"
    )
    port map (
        clk => clk,
        enable => io_rden_p,
        data_out => io_data_p
    );
    
    iobuffer_secondary : io_buffer
    generic map (
        text_file => "../../text/input_text_secondary.txt"
    )
    port map (
        clk => clk,
        enable => io_rden_s,
        data_out => io_data_s
    );
    
    imem_primary : instruction_memory_primary port map (
        addr => inst_addr_p,
        iout => inst_data_p
    );
    
    imem_secondary : instruction_memory_secondary port map (
        addr => inst_addr_s,
        iout => inst_data_s
    );
    
    shared_dmem : data_memory port map (
        clk => clk,
        rst => rst,
        din => bus_mem_data,
        dout => mem_data_read,
        addr => bus_mem_addr,
        wren => bus_mem_wren
    );
    
    shared_bus : data_bus port map (
        clk => clk,
        rst => rst,
        req(0) => bus_req_p,
        req(1) => bus_req_s,
        req_lock(0) => bus_lock_p,
        req_lock(1) => bus_lock_s,
        req_unlock(0) => bus_unlock_p,
        req_unlock(1) => bus_unlock_s,
        wren(0) => mem_wren_p,
        wren(1) => mem_wren_s,
        grant(0) => bus_grant_p,
        grant(1) => bus_grant_s,
        addr_in0 => mem_addr_p,
        addr_in1 => mem_addr_s,
        data_in0 => mem_dout_p,
        data_in1 => mem_dout_s,
        wren_out => bus_mem_wren,
        addr_out => bus_mem_addr,
        data_out => bus_mem_data
    );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state
      wait for 4.625 * clk_period;
      rst <= '0';

      wait;
   end process;

END;
