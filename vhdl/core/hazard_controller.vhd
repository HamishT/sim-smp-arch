----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:13:30 04/19/2017 
-- Design Name: 
-- Module Name:    hazard_controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hazard_controller is
    Port ( enable : in  STD_LOGIC;
           id_a_addr : in  STD_LOGIC_VECTOR (3 downto 0);
           id_b_addr : in  STD_LOGIC_VECTOR (3 downto 0);
           ex_wr_addr : in  STD_LOGIC_VECTOR (3 downto 0);
           fwd_sel_a : out  STD_LOGIC;
           fwd_sel_b : out  STD_LOGIC);
end hazard_controller;

architecture Behavioral of hazard_controller is

    -- delay reasoning: same as "control unit"
    constant T_DELAY : time := 1.5 ns;
    
begin

    -- fwd when referencing a non-hardwired register in ID with its unwritten result in EX
    -- enable should be set if current instruction would perform a register writeback
    
    fwd_sel_a <= '1' when enable = '1'
                      and id_a_addr(3 downto 1) /= "000"
                      and id_a_addr = ex_wr_addr
                     else '0' after T_DELAY;
    fwd_sel_b <= '1' when enable = '1'
                      and id_b_addr(3 downto 1) /= "000"
                      and id_b_addr = ex_wr_addr
                      else '0' after T_DELAY;

end Behavioral;

