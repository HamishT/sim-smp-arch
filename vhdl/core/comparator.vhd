----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:18:13 04/12/2017 
-- Design Name: 
-- Module Name:    comparator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparator is
    Port ( arg_a : in  STD_LOGIC_VECTOR (7 downto 0);
           arg_b : in  STD_LOGIC_VECTOR (7 downto 0);
           lt : out  STD_LOGIC;
           eq : out  STD_LOGIC);
end comparator;

architecture Behavioral of comparator is
    constant T_LT : time := 1ns; -- undetermined: T = 0.2ns per logic level
    constant T_EQ : time := 1ns; --
    signal ua, ub : signed(7 downto 0);
begin
    ua <= signed(arg_a);
    ub <= signed(arg_b);

    lt <= '1' when ua < ub else '0' after T_LT;
    eq <= '1' when ua = ub else '0' after T_EQ;

end Behavioral;

