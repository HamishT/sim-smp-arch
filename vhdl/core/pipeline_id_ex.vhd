----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:31:45 04/12/2017 
-- Design Name: 
-- Module Name:    pipeline_id_ex - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_id_ex is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           en : in  STD_LOGIC;
           zero : in  STD_LOGIC;
           
           next_reg_a_data : in  STD_LOGIC_VECTOR (7 downto 0);
           next_reg_b_data : in  STD_LOGIC_VECTOR (7 downto 0);
           next_imm_val : in  STD_LOGIC_VECTOR (7 downto 0);
           next_reg_wr_addr : in  STD_LOGIC_VECTOR (3 downto 0);
           next_sub : in  STD_LOGIC;
           next_use_imm : in  STD_LOGIC;
           next_mem_rden :  in  STD_LOGIC;
           next_mem_wren :  in  STD_LOGIC;
           next_lock : in  STD_LOGIC;
           next_unlock : in  STD_LOGIC;
           next_mem_to_reg :  in  STD_LOGIC;
           next_reg_wren : in  STD_LOGIC;
           next_pc_rel : in  STD_LOGIC_VECTOR (6 downto 0);
           next_beq : in  STD_LOGIC;
           next_bne : in  STD_LOGIC;
           next_blt : in  STD_LOGIC;
           
           curr_reg_a_data : out  STD_LOGIC_VECTOR (7 downto 0);
           curr_reg_b_data : out  STD_LOGIC_VECTOR (7 downto 0);
           curr_imm_val : out  STD_LOGIC_VECTOR (7 downto 0);
           curr_reg_wr_addr : out  STD_LOGIC_VECTOR (3 downto 0);
           curr_sub : out  STD_LOGIC;
           curr_use_imm : out  STD_LOGIC;
           curr_mem_rden : out  STD_LOGIC;
           curr_mem_wren : out  STD_LOGIC;
           curr_lock : out  STD_LOGIC;
           curr_unlock : out  STD_LOGIC;
           curr_mem_to_reg : out  STD_LOGIC;
           curr_reg_wren : out  STD_LOGIC;
           curr_pc_rel : out  STD_LOGIC_VECTOR (6 downto 0);
           curr_beq : out  STD_LOGIC;
           curr_bne : out  STD_LOGIC;
           curr_blt : out  STD_LOGIC);
end pipeline_id_ex;

architecture Behavioral of pipeline_id_ex is
    -- 46 register bits
    signal mem : std_logic_vector(45 downto 0);
begin

    curr_reg_a_data <= mem(7 downto 0);
    curr_reg_b_data <= mem(15 downto 8);
    curr_imm_val <= mem(23 downto 16);
    curr_reg_wr_addr <= mem(27 downto 24);
    curr_sub <= mem(28);
    curr_use_imm <= mem(29);
    curr_mem_rden <= mem(30);
    curr_mem_wren <= mem(31);
    curr_lock <= mem(32);
    curr_unlock <= mem(33);
    curr_mem_to_reg <= mem(34);
    curr_reg_wren <= mem(35);
    curr_pc_rel <= mem(42 downto 36);
    curr_beq <= mem(43);
    curr_bne <= mem(44);
    curr_blt <= mem(45);

    process(clk)
    begin
        if rst = '1' then
            mem <= (others => '0');
        elsif rising_edge(clk) then
            if en = '1' then
                if zero = '1' then
                    mem <= (others => '0');
                else
                    -- note these are in reverse order to the curr_ signals
                    mem <= next_blt
                         & next_bne
                         & next_beq
                         & next_pc_rel
                         & next_reg_wren
                         & next_mem_to_reg
                         & next_unlock
                         & next_lock
                         & next_mem_wren
                         & next_mem_rden
                         & next_use_imm
                         & next_sub
                         & next_reg_wr_addr
                         & next_imm_val
                         & next_reg_b_data
                         & next_reg_a_data;
                end if;
            end if;
        end if;
    end process;

end Behavioral;

