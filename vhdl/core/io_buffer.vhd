----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:33:10 04/10/2017 
-- Design Name: 
-- Module Name:    io_buffer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.TEXTIO.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity io_buffer is
    Generic ( text_file : string := "" );
    Port ( clk : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           data_out : out std_logic_vector(7 downto 0) );
end io_buffer;

architecture Behavioral of io_buffer is

    type mem_t is array (0 to 1023) of std_logic_vector(7 downto 0);

    impure function mem_init (constant text_file : in string) return mem_t is
        file infile : text is in text_file;
        variable inline : line;
        variable inchar : character;
        variable outmem : mem_t := (others => (others => '0'));
        variable index : integer := 0;
    begin
        readline(infile, inline);
        while inline'length /= 0 loop
            read(inline, inchar);
            outmem(index) := std_logic_vector(to_unsigned(character'pos(inchar), 8));
            index := index + 1;
        end loop;
        return outmem;
    end function;
    
    signal mem : mem_t := mem_init(text_file);
    signal counter : integer := 0;
    
begin 
 
    data_out <= mem(counter);
    
    read_proc: process(clk)
    begin
        if rising_edge(clk) and enable = '1' then
            counter <= counter + 1;
        end if;
    end process;

end Behavioral;

