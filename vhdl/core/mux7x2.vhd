----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:32:00 05/18/2017 
-- Design Name: 
-- Module Name:    mux7x2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux7x2 is
    Port ( a : in  STD_LOGIC_VECTOR (6 downto 0);
           b : in  STD_LOGIC_VECTOR (6 downto 0);
           s : in  STD_LOGIC;
           o : out  STD_LOGIC_VECTOR (6 downto 0));
end mux7x2;

architecture Behavioral of mux7x2 is
    constant T_SEL : time := 0.2 ns;
begin
    o <= b when s = '1' else a after T_SEL;
end Behavioral;

