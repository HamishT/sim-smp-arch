----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:45:37 04/11/2017 
-- Design Name: 
-- Module Name:    program_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity program_counter is
    generic ( WIDTH : natural := 7;
              BRANCH_WIDTH : natural := 4);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           en : in  STD_LOGIC;
           pcout : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           branch : in  STD_LOGIC;
           target : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end program_counter;

architecture Behavioral of program_counter is
    constant T_WR : time := 0.5ns; -- exact per spec
    signal pc : std_logic_vector(WIDTH-1 downto 0);
    signal pc_inc : std_logic_vector(WIDTH-1 downto 0);
begin

	-- TODO: add sub-component delays (adders and muxes)
    -- unsigned + signed is illegal: signed + signed should work
--    branch_target <= std_logic_vector(signed(saved_pc) + signed(target(BRANCH_WIDTH-1 downto 0)));
    pc_inc <= std_logic_vector(unsigned(pc) + 1);
    
    pcout <= pc after T_WR;

    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                pc <= (others => '0');
            elsif en = '1' then
                if branch = '1' then
                    pc <= target;
                else
                    pc <= pc_inc;
                end if;
            end if;
        end if;
    end process;

end Behavioral;

