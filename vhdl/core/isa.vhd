--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package isa is

-- type <new_type> is
--  record
--    <type_name>        : std_logic_vector( 7 downto 0);
--    <type_name>        : std_logic;
-- end record;
--
-- Declare constants
--
-- constant <constant_name>		: time := <time_unit> ns;
-- constant <constant_name>		: integer := <value;
--
-- Declare functions and procedure
--
-- function <function_name>  (signal <signal_name> : in <type_declaration>) return <type_declaration>;
-- procedure <procedure_name> (<type_declaration> <constant_name>	: in <type_declaration>);
--

    subtype instruction is std_logic_vector(15 downto 0);
    subtype nibble is std_logic_vector(3 downto 0);
    subtype offset is integer range -8 to 7;
    subtype target is integer range 0 to 127;
    subtype immediate is integer range 0 to 255;
    
    -- ##TAG_START_INST_DEFS##
	function inst_nop return instruction;
    function inst_add (constant rd, rs, rt : in nibble) return instruction;
    function inst_sub (constant rd, rs, rt : in nibble) return instruction;
    function inst_rio (constant rd : in nibble) return instruction;
    function inst_ldi (constant rd : in nibble; constant val: in immediate) return instruction; 
    function inst_lw (constant rd, rs : in nibble) return instruction;
    function inst_sw (constant rt, rs : in nibble) return instruction;
    function inst_lwl (constant rd, rs : in nibble) return instruction;
    function inst_swu (constant rt, rs : in nibble) return instruction;
    function inst_beq (constant rs, rt : in nibble; constant offs : in offset) return instruction;
    function inst_bne (constant rs, rt : in nibble; constant offs : in offset) return instruction;
    function inst_blt (constant rs, rt : in nibble; constant offs : in offset) return instruction;
    function inst_jmp (constant target: in target) return instruction;
    -- ##TAG_END_INST_DEFS##
    
    -- ##TAG_START_REG_DEFS##
    constant R_H0   : nibble := x"0";
    constant R_H1   : nibble := x"1";
    constant R_T0   : nibble := x"2";
    constant R_T1   : nibble := x"3";
    constant R_T2   : nibble := x"4";
    constant R_T3   : nibble := x"5";
    constant R_T4   : nibble := x"6";
    constant R_T5   : nibble := x"7";
    constant R_T6   : nibble := x"8";
    constant R_T7   : nibble := x"9";
    constant R_T8   : nibble := x"a";
    constant R_T9   : nibble := x"b";
    constant R_T10  : nibble := x"c";
    constant R_T11  : nibble := x"d";
    constant R_T12  : nibble := x"e";
    constant R_T13  : nibble := x"f";
    -- ##TAG_END_REG_DEFS##
    
    -- internal stuff (don't refer to these directly when writing assembly)
    
    constant NUL4 : std_logic_vector(3 downto 0) := (others => '0'); -- for reg ignore
    constant NUL5 : std_logic_vector(4 downto 0) := (others => '0'); -- for JMP
    constant NUL8 : std_logic_vector(7 downto 0) := (others => '0'); -- for RIO

    constant OP_NOP : nibble := x"0";
    constant OP_ADD : nibble := x"1";
    constant OP_SUB : nibble := x"2";
    constant OP_RIO : nibble := x"3";
    constant OP_LDI : nibble := x"4";
    constant OP_LW  : nibble := x"5";
    constant OP_SW  : nibble := x"6";
    constant OP_LWL : nibble := x"7";
    constant OP_SWU : nibble := x"8";
    constant OP_BEQ : nibble := x"9";
    constant OP_BNE : nibble := x"a";
    constant OP_BLT : nibble := x"b";
    constant OP_JMP : nibble := x"c";
    
end isa;

package body isa is

    function inst_nop return instruction is begin
        return OP_NOP & NUL4 & NUL4 & NUL4;
    end function;

    -- arithmetic instruction generators

    function inst_add (constant rd, rs, rt : in nibble) return instruction is begin
        return OP_ADD & rd & rs & rt;
    end function;
    
    function inst_sub (constant rd, rs, rt : in nibble) return instruction is begin
        return OP_SUB & rd & rs & rt;
    end function;
    
    function inst_rio (constant rd : in nibble) return instruction is begin
        return OP_RIO & rd & NUL4 & NUL4;
    end function;
    
    -- immediate generator
    
    function inst_ldi (constant rd : in nibble; constant val : in immediate) return instruction is begin
        return OP_LDI & rd & std_logic_vector(to_unsigned(val, 8));
    end function;
    
    -- memory instruction generators
    
    function inst_lw (constant rd, rs : in nibble) return instruction is begin
		return OP_LW & rd & rs & NUL4;
    end function;
    
    function inst_sw (constant rt, rs : in nibble) return instruction is begin
        return OP_SW & NUL4 & rs & rt;
    end function;
    
    function inst_lwl (constant rd, rs : in nibble) return instruction is begin
        return OP_LWL & rd & rs & NUL4;
    end function;
    
    function inst_swu (constant rt, rs : in nibble) return instruction is begin
        return OP_SWU & NUL4 & rs & rt;
    end function;
     
    -- branch instruction generators
    
    function inst_beq (constant rs, rt : in nibble; constant offs : in offset) return instruction is begin
        return OP_BEQ & std_logic_vector(to_signed(offs, 4)) & rs & rt;
    end function;
    
    function inst_bne (constant rs, rt : in nibble; constant offs : in offset) return instruction is begin
        return OP_BNE & std_logic_vector(to_signed(offs, 4)) & rs & rt;
    end function;
    
    function inst_blt (constant rs, rt : in nibble; constant offs : in offset) return instruction is begin
        return OP_BLT & std_logic_vector(to_signed(offs, 4)) & rs & rt;
    end function;
    
    function inst_jmp (constant target : in target) return instruction is begin
        return OP_JMP & NUL5 & std_logic_vector(to_unsigned(target, 7));
    end function;

---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end isa;
