----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:23:46 04/18/2017 
-- Design Name: 
-- Module Name:    pipeline_ex_wb - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_ex_wb is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           en : in  STD_LOGIC;
           zero : in  STD_LOGIC;
           
           next_reg_data : in std_logic_vector(7 downto 0);
           next_reg_addr : in std_logic_vector(3 downto 0);
           next_reg_wren : in std_logic;
           
           curr_reg_data : out std_logic_vector(7 downto 0);
           curr_reg_addr : out std_logic_vector(3 downto 0);
           curr_reg_wren : out std_logic);
end pipeline_ex_wb;

architecture Behavioral of pipeline_ex_wb is
    signal mem : std_logic_vector(12 downto 0);
begin
    
    curr_reg_data <= mem(12 downto 5);
    curr_reg_addr <= mem(4 downto 1);
    curr_reg_wren <= mem(0);
    
    process(clk, rst)
    begin
        if rst = '1' then
            mem <= (others => '0');
        elsif rising_edge(clk) then
            if en = '1' then
                if zero = '1' then
                    mem <= (others => '0');                    
                else
                    mem <= next_reg_data
                         & next_reg_addr
                         & next_reg_wren;
                end if;
            end if;
        end if;
    end process;

end Behavioral;

