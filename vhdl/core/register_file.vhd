----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:01:55 04/12/2017 
-- Design Name: 
-- Module Name:    register_file - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity register_file is
    generic ( SIZEB : natural := 4; -- must = log2(sizeT)
              SIZET : natural := 16; -- must = 2^sizeB
              WIDTH : natural := 8);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           addr_rd_a : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
           addr_rd_b : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
           addr_wr : in  STD_LOGIC_VECTOR (SIZEB-1 downto 0);
           wren : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           dout_a : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0);
           dout_b : out  STD_LOGIC_VECTOR (WIDTH-1 downto 0));
end register_file;

architecture Behavioral of register_file is
    constant T_RD : time := 0.7ns; -- spec: T for 8N registers = 0.3N + 0.4log2(N) [N = 2 for 16 registers]
    constant T_WR : time := 0.7ns; --
    type mem_t is array(0 to SIZET-1) of std_logic_vector(WIDTH-1 downto 0);
    signal mem : mem_t;
begin

    dout_a <= mem(to_integer(unsigned(addr_rd_a))) after T_RD;
    dout_b <= mem(to_integer(unsigned(addr_rd_b))) after T_RD;

    proc_wr: process(clk)
    begin
        if falling_edge(clk) then
            if rst = '1' then
                mem <= (0      => std_logic_vector(to_unsigned(0, WIDTH)),
                        1      => std_logic_vector(to_unsigned(1, WIDTH)),
                        others => (others => '0'));
            elsif wren = '1' and unsigned(addr_wr) > 1 then -- ignore writes for hard-coded
                mem(to_integer(unsigned(addr_wr))) <= din after T_WR;
            end if;
        end if;
    end process;
                

end Behavioral;

