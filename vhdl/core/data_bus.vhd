----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:52:25 05/08/2017 
-- Design Name: 
-- Module Name:    data_bus - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_bus is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           req : in  STD_LOGIC_VECTOR (1 downto 0);
           req_lock : in  STD_LOGIC_VECTOR (1 downto 0);
           req_unlock : in  STD_LOGIC_VECTOR (1 downto 0);
           wren : in  STD_LOGIC_VECTOR (1 downto 0);
           grant : out  STD_LOGIC_VECTOR (1 downto 0);
           addr_in0 : in  STD_LOGIC_VECTOR (7 downto 0);
           addr_in1 : in  STD_LOGIC_VECTOR (7 downto 0);
           data_in0 : in  STD_LOGIC_VECTOR (7 downto 0);
           data_in1 : in  STD_LOGIC_VECTOR (7 downto 0);
           wren_out : out  STD_LOGIC;
           addr_out : out  STD_LOGIC_VECTOR (7 downto 0);
           data_out : out  STD_LOGIC_VECTOR (7 downto 0));
end data_bus;

architecture Behavioral of data_bus is
    constant T_GRANT : time := 0.4 ns; -- equivalent to a 4to1 mux or 2 gate levels
    signal muxz, muxa, muxb, muxo : std_logic_vector(18 downto 0);
    signal locked_to : std_logic_vector(1 downto 0) := "00";
begin

    -- bus inputs from cores
    muxz <= (others => '0');
    muxa <= "01" & wren(0) & addr_in0 & data_in0;
    muxb <= "10" & wren(1) & addr_in1 & data_in1;
    
    -- bus output prioritises lock, then core 0, then core 1 in that order
    muxo <= muxa when locked_to(0) = '1' else
            muxb when locked_to(1) = '1' else
            muxa when req(0) = '1' else
            muxb when req(1) = '1' else
            muxz after T_GRANT;
    
    -- bus outputs
    grant <= muxo(18 downto 17);
    wren_out <= muxo(16);
    addr_out <= muxo(15 downto 8);
    data_out <= muxo(7 downto 0);
    
    -- essentially:
    -- if no lock, give it to 0 then 1 then no-one, in that priority order;
    -- if locked, and unlock is asserted, set lock to neither, or the
    -- other core if it requests lock at the same time
    lock_proc: process(clk, locked_to)
    begin
        if rst = '1' then
            locked_to <= "00";
        elsif rising_edge(clk) then
            if locked_to(0) = '1' then
                if req_unlock(0) = '1' then
                    if req_lock(1) = '1' then
                        locked_to(0) <= '0';
                        locked_to(1) <= '1';
                    else
                        locked_to <= "00";
                    end if;
                end if;
            elsif locked_to(1) = '1' then
                if req_unlock(1) = '1' then
                    if req_lock(0) = '1' then
                        locked_to(0) <= '1';
                        locked_to(1) <= '0';
                    else
                        locked_to <= "00";
                    end if;
                end if;
            elsif req_lock(0) = '1' then
                locked_to(0) <= '1';
            elsif req_lock(1) = '1' then
                locked_to(1) <= '1';
            end if;
        end if;
    end process;

end Behavioral;

