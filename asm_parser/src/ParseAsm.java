import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ParseAsm {

	private enum ArgType {
		REGISTER, JUMP_OFFSET, JUMP_TARGET, IMMEDIATE
	}

	private static Set<String> regDefs = new HashSet<String>();
	private static Map<String, List<ArgType>> instDefs = new HashMap<String, List<ArgType>>();

	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("Usage: java -jar asm_parse.jar <isa> <input> <output>");
			System.exit(1);
		}
		try {
			FileInputStream fis = new FileInputStream(new File(args[1]));
			ByteArrayOutputStream bufferOutStream = new ByteArrayOutputStream();
			BufferedReader fileReader = new BufferedReader(new InputStreamReader(fis));
			PrintWriter bufferWriter = new PrintWriter(bufferOutStream);

			System.out.println("Beginning ISA definitions parse...");
			getDefinitions(args[0]);

			System.out.println("Beginning assembly parse...");
			parse(fileReader, bufferWriter);

			System.out.println("Attempting instruction memory replace...");
			ByteArrayInputStream bufferInStream = new ByteArrayInputStream(bufferOutStream.toByteArray());
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(bufferInStream));
			replaceInstructions(args[2], bufferReader);

			System.out.println("Parse completed.");
		} catch (IOException e) {
			System.err.println("Parser IO error: " + e.getMessage());
		}
	}

	private static class Line {
		int num;
		String raw;
		String keyword;
		List<String> args = new ArrayList<String>();
		boolean isDeclaration = false;

		public Line(int num, String line) {
			this.num = num;
			raw = line;
			String[] tokens = line.split("\\s+");
			keyword = tokens[0];
			if (keyword.charAt(0) == '$' || keyword.charAt(0) == '%')
				isDeclaration = true;
			for (int i = 1; i < tokens.length; i++) {
				if (!tokens[i].trim().isEmpty())
					args.add(tokens[i]);
			}
		}
	}

	private static void getDefinitions(String fname) throws IOException {
		final String INST_START = "##TAG_START_INST_DEFS##";
		final String INST_END = "##TAG_END_INST_DEFS##";
		final String REG_START = "##TAG_START_REG_DEFS##";
		final String REG_END = "##TAG_END_REG_DEFS##";
		List<String> isaLines = Files.readAllLines(Paths.get(fname));

		TagSplitResult instSplit = splitByTag(isaLines, INST_START, INST_END);
		TagSplitResult regSplit = splitByTag(isaLines, REG_START, REG_END);

		/*
		 * This regex (?:\\((?<args>.*?)\\))? matches an optional string of
		 * anything inside parentheses, naming the contents 'args'. The
		 * outermost parentheses are a non-capture optional group, the
		 * second-innermost are parenthesis literals and the innermost are a
		 * named capture group reluctantly matching any string
		 */
		String instPString = "function +inst_(?<op>[a-z]+) *?(?:\\((?<args>.*?)\\))? +return +instruction;";
		Pattern instPattern = Pattern.compile(instPString);
		String argPString = "constant (?<argnames>.*?) ?: ?in (?<argtype>\\w+)";
		Pattern argPattern = Pattern.compile(argPString);

		for (String line : instSplit.subset) {
			Matcher ml = instPattern.matcher(line.trim());
			if (!ml.find())
				continue;
			String op = ml.group("op");
			List<ArgType> list = new ArrayList<ArgType>();
			instDefs.put(op, list);
			String args = ml.group("args");
			if (args == null)
				continue;
			Matcher ma = argPattern.matcher(args);
			while (ma.find()) {
				String names = ma.group("argnames");
				String type = ma.group("argtype");
				int numArgs = names.split(",").length;
				ArgType t = null;
				switch (type) {
				case "nibble":
					t = ArgType.REGISTER;
					break;
				case "offset":
					t = ArgType.JUMP_OFFSET;
					break;
				case "target":
					t = ArgType.JUMP_TARGET;
					break;
				case "immediate":
					t = ArgType.IMMEDIATE;
					break;
				default:
					fatal("Error: unknown type '" + type + "' in ISA function argument");
				}
				for (int i = 0; i < numArgs; i++) {
					list.add(t);
				}
			}
		}

		String regPString = "constant +R_(?<name>\\w+) +: +nibble";
		Pattern regPattern = Pattern.compile(regPString);
		for (String line : regSplit.subset) {
			Matcher ml = regPattern.matcher(line);
			if (!ml.find())
				continue;
			regDefs.add(ml.group("name"));
		}

	}

	private static void replaceInstructions(String fname, BufferedReader data) throws IOException {
		final String START_TAG = "##TAG_START_INST_MEM##";
		final String END_TAG = "##TAG_END_INST_MEM##";
		List<String> outLines = Files.readAllLines(Paths.get(fname));
		TagSplitResult split = splitByTag(outLines, START_TAG, END_TAG);

		PrintWriter pw = new PrintWriter(fname);
		System.out.println("Note: an error at this point might corrupt the output file.");

		// Stream.concat(split.prefix.stream(), Stream.concat(data.lines(),
		// split.suffix.stream())).forEach(pw::println);
		Stream.of(split.prefix.stream(), data.lines(), split.suffix.stream()).flatMap(s -> s).forEach(pw::println);

		pw.flush();
		pw.close();
	}

	private static class TagSplitResult {
		List<String> prefix;
		List<String> subset;
		List<String> suffix;

		TagSplitResult(List<String> prefix, List<String> subset, List<String> suffix) {
			this.prefix = prefix;
			this.subset = subset;
			this.suffix = suffix;
		}
	}

	private static TagSplitResult splitByTag(List<String> lines, String startTag, String endTag) {
		int startIndex = -1;
		int endIndex = -1;
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if (line.contains(startTag)) {
				if (startIndex != -1) {
					fatal("Error: multiple start tags found (" + startTag + ")");
				}
				startIndex = i;
			}
			if (line.contains(endTag)) {
				if (endIndex != -1) {
					fatal("Error: multiple end tags found (" + endTag + ")");
				}
				if (startIndex == -1 || startIndex == i) {
					fatal("Error: end tag found at or before start tag (" + endTag + ")");
				}
				endIndex = i;
			}
			// System.out.format("Start %d end %d line %s%n", startIndex,
			// endIndex, line);
		}

		if (startIndex < 0 || endIndex < 0 || endIndex <= startIndex) {
			fatal("Error: file tags missing or not placed properly: " + startTag + " and " + endTag);
		}

		return new TagSplitResult(lines.subList(0, startIndex + 1), lines.subList(startIndex + 1, endIndex),
				lines.subList(endIndex, lines.size()));
	}

	private static void parse(BufferedReader in, PrintWriter out) throws IOException {
		Map<String, Integer> labels = new HashMap<String, Integer>();
		Map<String, String> regvars = new HashMap<String, String>();
		String sline = null;

		List<Line> lines = new ArrayList<Line>();

		int linenum = 1;
		while ((sline = in.readLine()) != null) {
			int commentIndex = sline.indexOf('#');
			if (commentIndex > -1) {
				sline = sline.substring(0, commentIndex);
			}

			if (!sline.trim().isEmpty()) {
				lines.add(new Line(linenum, sline));
			}

			linenum++;
		}

		int first_pass_pc = 0;
		Iterator<Line> lit = lines.iterator();
		while (lit.hasNext()) {

			Line line = lit.next();
			String keyword = line.keyword;
			List<String> lineargs = line.args;

			if (!line.isDeclaration) {
				first_pass_pc++;
				continue;
			}

			// handle register variable definition
			if (keyword.charAt(0) == '$') {
				if (lineargs.size() != 1)
					fatal(line, "Instruction " + first_pass_pc + ": register variable definition syntax: '$var reg'");
				regvars.put(keyword.substring(1), lineargs.get(0));
			}
			// handle label definition
			else if (keyword.charAt(0) == '%') {
				if (lineargs.size() != 0)
					fatal(line, "Instruction " + first_pass_pc + ": label definition does not take any args");
				labels.put(keyword.substring(1), first_pass_pc);
			}
		}

		out.println("    signal mem : mem_t := (");

		int second_pass_pc = 0;
		lit = lines.iterator();
		while (lit.hasNext()) {
			Line line = lit.next();
			String keyword = line.keyword;
			List<String> lineargs = line.args;

			if (line.isDeclaration)
				continue;

			// handle instruction
			// prepare current instruction

			List<ArgType> argtypes = instDefs.get(keyword);
			if (argtypes == null)
				fatal(line, "Instruction '" + keyword + "' not recognised.");
			List<String> instargs = new ArrayList<String>();
			if (lineargs.size() != argtypes.size()) {
				String syntax = keyword + " "
						+ String.join(" ", argtypes.stream().map(ArgType::toString).toArray(String[]::new));
				fatal(line, "Bad argument count. Expected syntax: " + syntax);
			}
			for (int i = 0; i < lineargs.size(); i++) {
				String arg = lineargs.get(i);
				ArgType type = argtypes.get(i);
				char sigil = arg.charAt(0);
				String sub = arg.substring(1);
				if (sigil == '%') {
					if (!labels.containsKey(sub)) {
						fatal(line, "Label '" + sub + "' could not be resolved.");
					}
					int target = labels.get(sub);
					boolean absolute = keyword.equals("jmp");
					if (absolute && type != ArgType.JUMP_TARGET || !absolute && type != ArgType.JUMP_OFFSET) {
						String actual = (absolute) ? "jump target" : "branch offset";
						argumentTypeFatal(line, keyword, i, type, actual);
					}
					if (!absolute) {
						target -= second_pass_pc;
						if (target < -8 || target > 7) {
							fatal(line, "Label '" + sub + "' range (-8..+7) exceeded, got " + target + ".");
						}
					}
					instargs.add(String.valueOf(target));
				} else if (sigil == '!') {
					if (type != ArgType.IMMEDIATE)
						argumentTypeFatal(line, keyword, i, type, "immediate value");
					instargs.add(arg.substring(1));
				} else {
					if (type != ArgType.REGISTER)
						argumentTypeFatal(line, keyword, i, type, "register or register variable");
					if (sigil == '$') {
						String reg = regvars.get(sub);
						if (reg == null) {
							fatal(line, "Could not resolve register variable '" + sub + "'.");
						}
						arg = reg;
					}
					if (!regDefs.contains(arg))
						fatal(line, arg + " is not a valid register name.");
					instargs.add("R_" + arg);
				}
			}
			if (keyword.equals("nop")) {
				out.format("        %02d => inst_nop,%n", second_pass_pc);
			} else {
				out.format("        %02d => inst_%s(", second_pass_pc, keyword);
				Iterator<String> ait = instargs.iterator();
				while (ait.hasNext()) {
					out.print(ait.next());
					if (ait.hasNext()) {
						out.print(", ");
					}
				}
				out.println("),");
			}
			second_pass_pc++;
		}

		out.println("        others => x\"0000\"");
		out.println("    );");
		out.flush();
		in.close();
		out.close();

	}

	static private void argumentTypeFatal(Line line, String op, int index, ArgType expected, String actual) {
		fatal(line, "Instruction '" + op + "' expected type " + expected.toString() + " for argument " + index
				+ ", got " + actual + ".");
	}

	private static void fatal(String msg) {
		System.err.println(msg);
		System.exit(1);
	}

	private static void fatal(Line l, String msg) {
		System.err.println(msg);
		System.err.println("Error in line " + l.num + ": " + l.raw);
		System.exit(1);
	}

}
