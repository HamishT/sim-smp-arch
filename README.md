# Simulated SMP architecture #

Modified public version of university project. Forms basis for [my parser/emulator side project] (https://bitbucket.org/HamishT/asm-emulator).
Requires Xilinx ISE 14.7

## Contents ##

/vhdl/core  
Processor ISE project. Loads some files text from /text during simulation. Note 'isa.vhd', which has ISA definitions and instruction binary generators (it's a package so it doesn't show up in ISE).

/text  
Includes asm program, input pattern+text and parser script. The script run_parse.bat directly replaces the memory initialise vector in instruction_memory.vhd with a parsed version of program.txt. It seems insane, but it means isa.vhd has authorityover encoding so other VHDL code can rely on those definitions.

/asm_parser  
The asm parser code and binary. **Hacky regex solution. See [asm-emulator](https://bitbucket.org/HamishT/asm-emulator) for a better one.**