$0 H0
$1 H1
$table T12
$counter T13
$pos T0
$cnd T1
$tmp T2
$char0 T3
$char1 T4
$ind0 T5
$ind1 T6
$i T7
$tchar T8
$pchar T9
$t T10

# initial setup
# array constants
ldi $ind0 !255
ldi $table !65
ldi $counter !130

%search_init
lwl $tmp $0
swu $1 $ind0
add $i $0 $0
rio $tchar

%search_loop
bne $tchar $0 %search_check_match
jmp %halt

%search_check_match
lw $pchar $i
bne $tchar $pchar %search_miss
jmp %check_word_match

%search_miss
add $ind0 $table $i
lw $t $ind0
# skip count = i - T[i] if branch succeeds
sub $tmp $i $t
blt $t $0 %search_min
add $i $t $0

%search_skip_loop
bne $tmp $0 %search_skip_end
sub $tmp $tmp $1
rio $tchar
jmp %search_skip_loop

%search_skip_end
jmp %search_loop

%search_min
add $i $0 $0
rio $tchar
jmp %search_loop

%check_word_match
# check the next pattern character for null
add $ind0 $i $1
lw $pchar $ind0
beq $pchar $0 %check_word_match_success
# pattern string not over: get next character ready
add $i $i $1
rio $tchar
jmp %search_loop

%check_word_match_success
lwl $tmp $counter
# M[counter] += 1
add $tmp $tmp $1
swu $tmp $counter
# i = T[i] if T[i] > 0
add $ind0 $table $i
lw $tmp $ind0
blt $0 $tmp %match_success_ff
add $i $0 $0
jmp %search_loop

%match_success_ff
add $i $tmp $0
jmp %search_loop

%halt
nop
jmp %halt